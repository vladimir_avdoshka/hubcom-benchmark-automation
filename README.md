# Hubcom Benchmark Automation

Hubcom Benchmark Automation mechanism is a part of Performance Project.

# Roadmap
 - Deployment of specified Hub And FO version. 
The Formats fabfile could be used for such a deployment. Etalon Data directory should be provided for each new run.
 -  Making required data-set for benchmark purpose. (for now we stick with Diamond Like data set, in future we should enhance it to support other platforms datasets)
To analyze the etalon DB schema of 15.1 and create a list of customers/branches/accounts to set up in incoming statements.
 - MT940 producer - the mechanism is done, to make it configurable
 - BAI + BAII + BAIST producer - to make configurable mechanism 
 - AFB120 producer - to make configurable mechanism 
 - Launching the test via running scheduled jobs for Statement.cmd and other services on demand(typeperf for example). To make the working mechanism configurable.
 - Backing up the data folder and running the Kanban Parsing Script to obtain statistics on the run.
 - to review Kanban Parsing tool, make it configurable.
 - launching the Kanban parsing tool and backing up the results 
 
### Version
0.1

### Dependencies

 - Python 2.7.6 x86
 - mock (1.3.0) for unit-tests