from hubench.configs import launcher as configLauncher
from hubench.dispatch import launcher as dispatchLauncher
from hubench.hub import launcher as hubLauncher
from hubench.generate import launcher as generateLauncher


def generateTestData(benchmarkConf, dispatchingProcessor):
    for formatConf in benchmarkConf.FORMATS_PROFILE_TO_EXECUTE:
        formatDispatcher = dispatchingProcessor.getDispatcher(formatConf)
        formatGenerator = generateLauncher.createGenerator(formatConf)
        while formatGenerator.alive:
            formatDispatcher.dispatch(formatGenerator(formatDispatcher.accounts))


def main(scenarioName):
    appConf, huConf, benchmarkConf = configLauncher.getGlobalConfig(scenarioName)
    dispatchingProcessor = dispatchLauncher.getDispatchingProcessor(huConf, appConf.CUSTOMER_BRANCH_ACCOUNT_SRC)
    generateTestData(benchmarkConf, dispatchingProcessor)
    hubLauncher.bsiTest(appConf, huConf, dispatchingProcessor.getPusher())
