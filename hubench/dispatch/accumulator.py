"""
A mechanism to accumulate provide metadata for format-specific generators
such as file path, list of account to use in file
"""

import os
import time
from collections import namedtuple
import json
from random import choice
from .tools import ensureDir

CUSTOMER_BRUNCH_ACC_FABRIC = namedtuple('Unique_Combination_From_Customer_Branch_and_Account',
                                        ('customer', 'branch', 'account'))


class FileSnapshotMetadata(object):
    def __init__(self, fPath, fName, customer, branch, proto):
        self.fPath = fPath
        self.fName = fName
        self.customer = customer
        self.branch = branch
        self.proto = proto


FILE_NAME_TEMPL = '{protocol}{format_}{id}{cust}.{br}.S{ts}'


class AccumulatorException(RuntimeError):
    pass


def loadSource(path):
    with open(path, 'r') as src:
        return json.loads(src.read())


def getUniqueCustomerBranchAccount(fPath):
    assert os.path.exists(fPath)
    custBranchAccComboList = []
    for cust, allBranches in loadSource(fPath).iteritems():
        for branch, accList in allBranches.iteritems():
            custBranchAccComboList.append(CUSTOMER_BRUNCH_ACC_FABRIC(cust, branch, tuple(accList)))
    return custBranchAccComboList


class Dispatcher(object):
    def __init__(self, formatConf, protocolToInbox, customerBranchAccPath):
        self.formatConf = formatConf
        self.protoToInbox = protocolToInbox
        self.customerBranchAccList = getUniqueCustomerBranchAccount(customerBranchAccPath)
        self.customer, self.branch, self.proto = '', '', ''
        self.counter = iter(self.__limitedCounter())
        self.flLinks = set()

    def dispatch(self, text):
        fPath = self._dispatchFile(text)
        self.flLinks.add(FileSnapshotMetadata(fPath, os.path.basename(fPath), self.customer, self.branch, self.proto))

    @property
    def accounts(self):
        # TODO: to implement customer and branches mixing
        self.customer, self.branch, accList = choice(self.customerBranchAccList)

        def infiniteAccounts():
            while True:
                yield choice(accList)

        return infiniteAccounts()

    def _getDestinationDirAndNamePath(self):
        self.proto, inboxPath = choice(self.protoToInbox.items())
        format_ = self.formatConf.NAME.ljust(6, '_').lower()
        fileName = FILE_NAME_TEMPL.format(protocol=self.proto,
                                          format_=format_,
                                          id="%03d" % self.counter.next(),
                                          cust=self.customer,
                                          br=self.branch,
                                          ts=time.strftime('%Y%m%d%H%M%S'))
        return inboxPath, fileName

    def _dispatchFile(self, text):
        dstDir, dstNm = self._getDestinationDirAndNamePath()
        ensureDir(dstDir)
        fPath = os.path.join(dstDir, dstNm)
        with open(fPath, 'w') as outFile:
            outFile.write(text)
        return fPath

    def __limitedCounter(self, limit=1000):
        while True:
            for i in xrange(limit):
                yield i
            time.sleep(1)
