import os
import errno


def ensureDir(dirPath):
    if not os.path.exists(dirPath):
        try:
            os.makedirs(dirPath)
        except OSError as ex:
            if ex.errno == errno.EEXIST or os.path.isdir(dirPath):
                pass
            else:
                raise
