from .accumulator import Dispatcher
from .push import Pusher


def getDispatchingProcessor(huConf, customerBranchAccSource):
    return DispatchingProcessor(huConf, customerBranchAccSource)


class DispatchingProcessor(object):
    def __init__(self, huConfig, customerBranchAccSource):
        self.huConfig = huConfig
        self.customerBranchAccSource = customerBranchAccSource
        self.dispatchers = []
        self.pushers = []

    def getPusher(self):
        return lambda: [pusher.push() for pusher in self.pushers]

    def __setTraceToPusher(self, formatConf, dispatcher):
        self.pushers.append(Pusher(formatConf, dispatcher.flLinks, self.huConfig))

    def getDispatcher(self, formatConf):
        dp = Dispatcher(formatConf, self.huConfig.PROTOCOL_TO_INBOX, self.customerBranchAccSource)
        self.dispatchers.append(dp)
        self.__setTraceToPusher(formatConf, dp)
        return dp
