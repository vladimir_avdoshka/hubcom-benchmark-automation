"""
A mechanism which is dispatching files from staging directory
to HubCom InRoots
"""

import os
import time
from random import randint
import shutil
import multiprocessing
from .tools import ensureDir


class Pusher(object):
    def __init__(self, formatConf, flLinks, huConfig):
        self.formatName = formatConf.NAME
        self.minTimout = formatConf.MIN_FILES_PER_MINUTE
        self.maxTimout = formatConf.MAX_FILES_PER_MINUTE
        self.hubDataRoot = huConfig.DATA_ROOT
        self.protoToInbox = huConfig.PROTOCOL_TO_RELATIVE_INBOX
        self.flLinks = flLinks

    @property
    def timout(self):
        return randint(self.minTimout, self.maxTimout)

    def waitTimout(self):
        for x in xrange(self.timout):
            time.sleep(1)

    def push(self):
        p = multiprocessing.Process(target=self._push)
        p.start()
        return p

    def _push(self):
        # start new process
        for fl in self.flLinks:
            self.waitTimout()
            self._createKanban(fl)
            self._transferFile(fl)

    def _getDestinationPath(self, fl):
        formatFolder = self.formatName.upper()  # TODO: to add _concentrate postfix if needed
        return os.path.join(self.hubDataRoot,
                            self.protoToInbox[fl.proto],
                            formatFolder)

    def _transferFile(self, fl):
        dstPath = self._getDestinationPath(fl)
        ensureDir(dstPath)
        shutil.copy(fl.fPath, os.path.join(dstPath, fl.fName))

    def _createKanban(self, fl):
        ensureDir(os.path.join(self.hubDataRoot, 'Kanban', 'kStaIn'))  # FIXME: why it is not created yet?
        with open(os.path.join(self.hubDataRoot, 'Kanban', 'kStaIn', fl.fName), 'w') as kanbanFile:
            kanbanFile.write('\n'.join([
                "%s testlogjnl testsignet" % (">-" * 20),
                "ORIGIN = benchmarking",
                "CREATE_STAMP = %s" % time.strftime("%Y/%m/%d %H:%M:%S"),
                "FILE_COLLECTED = %s" % fl.fName,
                "CUSTOMER = %s" % fl.customer,
                "BRANCH = %s" % fl.branch,
                "WAY = %s" % fl.proto,
                "FORMAT = %s" % self.formatName.upper()
            ]))
