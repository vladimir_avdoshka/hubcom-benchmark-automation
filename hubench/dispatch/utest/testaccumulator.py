from mock import patch
from unittest import TestCase
from hubench.dispatch.accumulator import Dispatcher


class DispatcherTests(TestCase):
    def setUp(self):
        class ConfigDict(object):
            pass

        self.formConfig = ConfigDict()
        self.formConfig.NAME = 'MT940'

    def testInitialization(self):
        with patch('hubench.dispatch.accumulator.getUniqueCustomerBranchAccount', lambda x: None):
            d = Dispatcher(self.formConfig, {}, '')
