# -*- coding: utf-8 -*-

DATA_ROOT_BKP = r"c:\TEST\bkp"  # where to bkp fs after test
DATA_ROOT_SAMPLE = r"c:\vagrant\gk13\HU\datasample\data.zip"
GENERATED_FILES_ROOT_FOLDER = r"c:\TEST\load_data"  # where to put generated files
ASSETS_FOLDER = r".\hubench\assets"

DATA_ROOT = "c:\\HubCom\\data\\"
HU_SCRIPTS = "c:\\HubCom\\scripts\\"

SCHEDULED_STATEMENT_TASK = "Statements"
