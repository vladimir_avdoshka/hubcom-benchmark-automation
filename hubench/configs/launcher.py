'''
Copyright 2000 - 2015 Kyriba Corp. All Rights Reserved.
The content of this file is copyrighted by Kyriba Corporation and can
not be reproduced, distributed, altered or used in any form, in whole or
in part.

Author: M-VAV

__version__ = {'Branch' : 'FO', 'File version' : '1'}
__history__ :
'''
from generic_config import GenericConfig


class ConfigConstructorException(RuntimeError):
    pass


def checkConfig(config, benchmarkProfileName):
    # TODO: to implement basic data validation
    try:
        appConfig = config.app
        huplatformConfig = config.huplatform
        benchmarkProfile = config.benchmarkProfile
    except AttributeError:
        raise ConfigConstructorException(
            'Test: `{}`. Missing one or more required configuration objects.'.format(benchmarkProfileName))
    config.app.TEST_NAME = benchmarkProfileName
    return config


def makeGlobalConfig(profileModule, benchmarkProfileName):
    try:
        prof = profileModule.BenchmarkProfile
    except AttributeError:
        raise ConfigConstructorException(
            'Test: `{}`. At least `BenchmarkProfile` Class should be defined in module `{}`.'.format(
                benchmarkProfileName, profileModule.__file__))
    app = getattr(profileModule, 'App', None)
    huplatform = getattr(profileModule, 'Huplatform', None)
    if huplatform is None and app is None:
        mergedConfig = GenericConfig(benchmarkProfile=prof)
    elif huplatform is None:
        mergedConfig = GenericConfig(benchmarkProfile=prof, app=app)
    elif app is None:
        mergedConfig = GenericConfig(benchmarkProfile=prof, huplatform=huplatform)
    else:
        mergedConfig = GenericConfig(benchmarkProfile=prof, huplatform=huplatform, app=app)
    return mergedConfig


def getConfigModule(benchmarkProfileName):
    module = '.'.join(('hubench', 'configs', 'profiles', benchmarkProfileName))
    try:
        return __import__(module, globals(), locals(), ['profiles', benchmarkProfileName])
    except ImportError:
        raise ConfigConstructorException(
            'Test: `{}`. Module `{}` was not found, please ensure it exists.'.format(benchmarkProfileName, module))


def splitConfig(config):
    return config.app, config.huplatform, config.benchmarkProfile


def getGlobalConfig(benchmarkProfileName):
    """

    :param scenarioNm: scenario which configuration to you like to run -> `profiles.profileName` e.g. `profile.diamondMt940`
    :return: Global `Config` Object -> generic_config.GenericConfig
    """
    mod = getConfigModule(benchmarkProfileName)
    conf = makeGlobalConfig(mod, benchmarkProfileName)
    conf = checkConfig(conf, benchmarkProfileName)
    return splitConfig(conf)
