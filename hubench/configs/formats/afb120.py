'''
Copyright 2000 - 2015 Kyriba Corp. All Rights Reserved.
The content of this file is copyrighted by Kyriba Corporation and can
not be reproduced, distributed, altered or used in any form, in whole or
in part.

Author: M-VAV

__version__ = {'Branch' : 'FO', 'File version' : '1'}
__history__ :
          vavdoshka xx/xx/xxxx - 
'''
from ..generic_config import Format


class DefaultProfile(Format):
    NAME = 'Afb120'
    MAX_FILES_PER_MINUTE = 9
    MIN_FILES_PER_MINUTE = 1
    MAX_FILE_SIZE = 50000
    MIN_FILE_SIZE = 300
    MIN_SIZE_PERCENT = 60
