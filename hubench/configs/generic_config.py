'''
Copyright 2000 - 2015 Kyriba Corp. All Rights Reserved.
The content of this file is copyrighted by Kyriba Corporation and can
not be reproduced, distributed, altered or used in any form, in whole or
in part.

Author: M-VAV

__version__ = {'Branch' : 'FO', 'File version' : '1'}
__history__ :
'''
import os
import json
import hubench.settings as settings


class AttrToDict(object):
    def __repr__(self):
        return self.renderConfig(asString=True)

    @classmethod
    def toDict(cls, asString=False):
        """
        represent a dictionary containing from all inherited and own class attributes
        :return: dictionary
        """
        result = cls._objectVariablesToDict()
        if asString:
            result = json.dumps(result, indent=4)
        return result

    @classmethod
    def _objectVariablesToDict(cls):
        """
        represent a dictionary containing from all inherited and own class attributes
        :return: dictionary
        """
        result = {}
        result.update(dict((key, value) for key, value in cls.__dict__.iteritems() if
                           not callable(value) and not key.startswith('_')
                           and key != 'toDict'))
        for base in cls.__bases__:
            if base is not object:
                result.update(base.toDict())
        return result


class App(AttrToDict):
    """
    Hubench tool specific configuration constants
    """
    DATA_ROOT_BKP = settings.DATA_ROOT_BKP
    DATA_ROOT_SAMPLE = settings.DATA_ROOT_SAMPLE
    GENERATED_FILES_ROOT_FOLDER = settings.GENERATED_FILES_ROOT_FOLDER
    ASSETS_FOLDER = settings.ASSETS_FOLDER
    CUSTOMER_BRANCH_ACCOUNT_SRC = os.path.join(ASSETS_FOLDER, 'customersBranchesAccountsFiltered.txt')
    SCHEDULED_STATEMENT_TASK = settings.SCHEDULED_STATEMENT_TASK
    TYPE_PERF_CMD = ('typeperf -o {report} "\processor(_Total)\% Processor Time" '
                     '"\processor(_Total)\% User Time" '
                     '"\processor(_Total)\% Privileged Time" '
                     '"\System\Processes" '
                     '"\System\Threads" '
                     '"\Memory\Available bytes" '
                     '"\Process(python)\% Processor Time" '
                     '"\Process(python)\% User Time" '
                     '"\Process(python)\% Privileged Time"  '
                     '"\Process(python)\Private Bytes" '
                     '"\Process(python)\IO Read Operations/sec" '
                     '"\Process(python)\IO Read Bytes/sec" '
                     '"\Process(python)\IO Write Operations/sec" '
                     '"\Process(python)\IO Write Bytes/sec" '
                     '"\Process(python)\IO Other Operations/sec" '
                     '"\Process(python)\IO Other Bytes/sec" '
                     '"\Process(python)\Thread Count"')


class Huplatform(AttrToDict):
    """
    Huplatform Specific Configuration Constants which will override config.py defaults
    """
    DATA_ROOT = settings.DATA_ROOT
    HU_SCRIPTS = settings.HU_SCRIPTS

    PROTOCOL_TO_RELATIVE_INBOX = {
        'RFTP': 'FTPIn',
        'MQHA': 'MQHA\in',
        'EDTR': 'EDITRAN\in',
        'EBCS': 'EBICS',
        'SAL2': 'SAL2\in'
    }

    DIR_FORMAT = {
        'AFB': 'Afb120',
        'AFBI': 'Af120I',
        'MT940': 'Mt940',
        'BAI': 'BAI',
        'BAII': 'BAII'
    }

    PROTOCOL_TO_INBOX = {k: os.path.join(App.GENERATED_FILES_ROOT_FOLDER, v)
                         for k, v in PROTOCOL_TO_RELATIVE_INBOX.items()}

    CONFIG_PATH = os.path.join(HU_SCRIPTS, 'huplatform', 'config.py')
    PYRO_START_CMD = "python %s" % os.path.join(HU_SCRIPTS, 'server', 'startup.py')
    CONCENTRATOR_CMD = "python %s" % os.path.join(HU_SCRIPTS, 'bsi', 'concentrator.py')


class Format(AttrToDict):
    """
    Generic Configuration for abstract BSI format
    """
    NAME = ''
    FILES_COUNT = 0
    PROFILING_FORMAT = ''
    MAX_FILES_PER_MINUTE = 0
    MIN_FILES_PER_MINUTE = 0
    MAX_FILE_SIZE = 0
    MIN_FILE_SIZE = 0
    MIN_SIZE_PERCENT = 0


class BenchmarkProfile(AttrToDict):
    """
    Formats Specific Configuration Constants
    """
    FORMATS_PROFILE_TO_EXECUTE = []  # formats profiles list should be specified
    CUSTOMER_BRANCH_ACC_SOURCE = 'mock'


class GenericConfig(object):
    """
    Combination of all configs makes a test representation
    """

    def __init__(self, app=App, huplatform=Huplatform, benchmarkProfile=BenchmarkProfile):
        self.app = app
        self.huplatform = huplatform
        self.benchmarkProfile = benchmarkProfile
