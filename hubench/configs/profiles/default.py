'''
Copyright 2000 - 2015 Kyriba Corp. All Rights Reserved.
The content of this file is copyrighted by Kyriba Corporation and can
not be reproduced, distributed, altered or used in any form, in whole or
in part.

Author: M-VAV

Templat which is using for unit-tests
'''

from ..generic_config import BenchmarkProfile
from ..formats import afb120, mt940


class Mt940Profile(mt940.DefaultProfile):
    FILES_COUNT = 100


class Afb120Profile(afb120.DefaultProfile):
    FILES_COUNT = 10


class BenchmarkProfile(BenchmarkProfile):
    FORMATS_PROFILE_TO_EXECUTE = [Mt940Profile, Afb120Profile]
