import mock
import unittest
from .. import launcher, generic_config
from ..profiles.default import Mt940Profile, Afb120Profile


class TestModuleWithProfile(object):
    class Profile(object):
        TEST = True
        FOO = 1


class TestModuleWithApp(TestModuleWithProfile):
    class App(object):
        TEST = True
        BAR = 'BAR'


class TestModuleWithHuplatform(TestModuleWithProfile):
    class Huplatform(object):
        TEST = True
        DUMMY = 'DUMMY'


class TestModuleWithHuplatformAndProfile(TestModuleWithApp, TestModuleWithHuplatform):
    pass


APP = TestModuleWithHuplatformAndProfile.App
HUPLATFORM = TestModuleWithHuplatformAndProfile.Huplatform
PROFILE = TestModuleWithHuplatformAndProfile.Profile
RENDER = ""'{\n    "APPLICATION_CONFIG": {\n        "TEST": true, \n        "BAR": "BAR"\n    },' \
         ' \n    "HUPLATFORM_CONFIG": {\n        "TEST": true, \n        "DUMMY": "DUMMY"\n    },' \
         ' \n    "PROFILE_CONFIG": {\n        "TEST": true, \n        "FOO": 1\n    }\n}'""


class TestGetConfig(unittest.TestCase):
    def testLoadConfigSanity(self):
        conf = launcher.getGlobalConfig('default')
        assertion = conf.benchmarkProfile.FORMATS_PROFILE_TO_EXECUTE
        self.assertTrue(len(assertion) == 2)
        self.assertTrue(Mt940Profile in assertion and Afb120Profile in assertion)
        #
        # def testLoadConfigSanityWithAfb120(self):
        #     conf = launcher.getConfig('diamondMt940')
        #     self.assertEqual(conf.profile.PROFILING_FORMAT, 'AFB120')
        #
        # def testLoadConfigExceptionInvalidName(self):
        #     self.assertRaises(launcher.ConfigConstructorException, launcher.getConfig, 'foobar')
        #
        # def testLoadConfigExceptionFailedImport(self):
        #     self.assertRaises(launcher.ConfigConstructorException, launcher.getConfig, 'foo.bar')
        #
        # def testLoadConfigSanityWithMt940andAfb120AtOnce(self):
        #     confAfb = launcher.getConfig('afb120.profile1')
        #     self.assertEqual(confAfb.profile.PROFILING_FORMAT, 'AFB120')
        #     confMt = launcher.getConfig('mt940.profile1')
        #     self.assertEqual(confMt.profile.PROFILING_FORMAT, 'MT940')
        #
        # def testLoadConfigProfile(self):
        #     with mock.patch('hubench.configs.launcher.getConfigModule', lambda x: TestModuleWithProfile):
        #         conf = launcher.getConfig('foo')
        #         self.assertTrue(conf.profile.TEST)
        #
        # def testLoadConfigApp(self):
        #     with mock.patch('hubench.configs.launcher.getConfigModule', lambda x: TestModuleWithApp):
        #         conf = launcher.getConfig('foo')
        #         self.assertTrue(conf.app.TEST)
        #
        # def testLoadConfigHuplatform(self):
        #     with mock.patch('hubench.configs.launcher.getConfigModule', lambda x: TestModuleWithHuplatform):
        #         conf = launcher.getConfig('foo')
        #         self.assertTrue(conf.huplatform.TEST)
        #
        # def testLoadConfigHuplatformAndProfileAndApp(self):
        #     with mock.patch('hubench.configs.launcher.getConfigModule', lambda x: TestModuleWithHuplatformAndProfile):
        #         conf = launcher.getConfig('foo')
        #         self.assertTrue(conf.profile.TEST)
        #         self.assertTrue(conf.huplatform.TEST)
        #         self.assertTrue(conf.app.TEST)

        # class GenericConfig(unittest.TestCase):
        #
        # def testConfigRenderingAsDict(self):
        #     config = generic_config.GenericConfig(app=APP, huplatform=HUPLATFORM, profile=PROFILE)
        #     self.assertEqual(config.renderConfig(), {'APPLICATION_CONFIG': {'BAR': 'BAR', 'TEST': True},
        #                                             'HUPLATFORM_CONFIG': {'DUMMY': 'DUMMY', 'TEST': True},
        #                                             'PROFILE_CONFIG': {'FOO': 1, 'TEST': True}})
        #
        # def testConfigRenderingAsPlainText(self):
        #     config = generic_config.GenericConfig(app=APP, huplatform=HUPLATFORM, profile=PROFILE)
        #     self.assertEqual(repr(config), RENDER)
