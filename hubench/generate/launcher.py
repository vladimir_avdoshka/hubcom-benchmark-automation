class FileGeneratorException(RuntimeError):
    pass


def getProducerModule(formatNm):
    formatNm = formatNm.lower()
    try:
        module = '.'.join(('hubench', 'generate', formatNm))
        producer = __import__(module, globals(), locals(), [formatNm])
    except ImportError:
        raise FileGeneratorException(
            'Could not import producer module {}. Please ensure module exists'.format(formatNm))
    return producer


def getProducer(formatName):
    producerModule = getProducerModule(formatName)
    try:
        return producerModule.Producer
    except AttributeError:
        raise FileGeneratorException('Producer Class should be defined in module {}'.format(producerModule.__name__))


def createGenerator(formatConf):
    formatFactory = getProducer(formatConf.NAME)
    return formatFactory(formatConf)
