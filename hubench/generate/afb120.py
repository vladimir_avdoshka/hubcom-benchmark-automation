from .generic_producer import GenericProducer

AFB_TEMPLATE = """
01%(banq)s    %(guichet)sEUR2 %(compte)s  160310                                                  0000000569119C
04%(banq)s0805%(guichet)sEUR2 %(compte)s  170310  170310*020868352798EDF PR SIMM BRED    0000000  0000000001551K001009
05EDF PR SIMM BRED NUM 001007 ECH 17.03.10 REF *020868352798 11305*178 E DF SIM
07%(banq)s    %(guichet)sEUR2 %(compte)s  170310                                                  0000000567568A
""".strip().replace('\r\n', '\n')

TEMPLATE_SIZE = 500
EOL = '\n'


class Producer(GenericProducer):
    @staticmethod
    def splitAcc(acc):
        banqPos = slice(0, 5)
        guichetPos = slice(5, 10)
        comptePos = slice(10, 21)
        banqPortion = acc[banqPos].ljust(5)
        guichetPortion = acc[guichetPos].ljust(5)
        comptePortion = acc[comptePos].ljust(11)
        return banqPortion, guichetPortion, comptePortion

    def __call__(self, accountGenerator):
        out = []
        for repeat in xrange(self.getFileSize() // TEMPLATE_SIZE):
            acc = accountGenerator.next()
            # while len(acc) > 21:
            #    acc = accountGenerator.next()
            banqPortion, guichetPortion, comptePortion = self.splitAcc(acc)
            txt = AFB_TEMPLATE % {'banq': banqPortion,
                                  'guichet': guichetPortion,
                                  'compte': comptePortion}
            out.append(EOL.join([line.ljust(120) for line in txt.split(EOL)]))
        self.counter += 1
        return EOL.join(out)
