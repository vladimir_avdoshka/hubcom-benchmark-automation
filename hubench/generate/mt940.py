import random
from .generic_producer import GenericProducer


class Mt940GeneratorException(RuntimeError):
    pass


EOL = '\r\n'
TEMPLATE_SIZE = 200  # byte avg
MIN_SEQ_SIZE = 3

MIN_FILE_SIZE = None
MAX_FILE_SIZE = None

MT_SINGLE_TEMPL = """
{1:F01BREDDEPPAKUS6390037018}{2:O9400430110302VBVBDE23242}{4:
:20:47306697500000
:25:%(acc)s
:28C:%(stmtNum)s
:60F:C151104EUR040001
:61:1301120112C10001BTC1Reference//8327000090031712
:86:333++4/
:62F:C151104EUR050001
-}
""".strip()

MT_OPEN_TEMPL = """
{1:F01BREDDEPPAKUS6390037018}{2:O9400430110302VBVBDE23242}{4:
:20:47306697500000
:25:%(acc)s
:28C:%(stmtNum)s
:60F:C151104EUR040001
:61:1301120112C10001BTC1Reference//8327000090031712
:86:333++4/
:62M:C151104EUR050001
-}
""".strip()

MT_MIDDLE_TEMPL = """
{1:F01BREDDEPPAKUS6390037018}{2:O9400430110302VBVBDE23242}{4:
:20:47306697500000
:25:%(acc)s
:28C:%(stmtNum)s
:60M:C151104EUR040001
:61:1301120112C10001BTC1Reference//8327000090031712
:86:333++4/
:62M:C151104EUR050001
-}
""".strip()

MT_FINAL_TEMPL = """
{1:F01BREDDEPPAKUS6390037018}{2:O9400430110302VBVBDE23242}{4:
:20:47306697500000
:25:%(acc)s
:28C:%(stmtNum)s
:60M:C151104EUR040001
:61:1301120112C10001BTC1Reference//8327000090031712
:86:333++4/
:62F:C151104EUR050001
-}
""".strip()


def getMtFileSize(minFileSize, maxFileSize):
    return random.randint(minFileSize, maxFileSize)


class Producer(GenericProducer):
    formatSpecificAttrs = ['SEQUENCE_DISTRIBUTION', 'SEQUENCE_MAX_SIZE']

    def __init__(self, formatConfig):
        super(Producer, self).__init__(formatConfig)
        self.singleCounter = 0
        assert self.config.SEQUENCE_MAX_SIZE >= MIN_SEQ_SIZE
        global MIN_FILE_SIZE, MAX_FILE_SIZE
        MIN_FILE_SIZE = self.config.MIN_FILE_SIZE
        MAX_FILE_SIZE = self.config.MAX_FILE_SIZE

    def __call__(self, accountGenerator):
        txt = ''
        if self.counter != 0 and self.counter % self.config.SEQUENCE_DISTRIBUTION == 0:
            sequenceLength = random.randint(MIN_SEQ_SIZE, self.config.SEQUENCE_MAX_SIZE)
            txt = self.sequencedMt(accountGenerator, sequenceLength)
            self.counter += sequenceLength
        else:
            txt = self.getSingleMt(accountGenerator)
            self.singleCounter += 1
            self.counter += 1
        return txt

    @property
    def randomStmtNum(self):
        return str(random.randint(1, 99999))

    def getFilledTemplate(self, templ, acc, stmNum):
        return templ % {'acc': acc, 'stmtNum': stmNum}

    def getSingleMt(self, accGen):
        global MT_SINGLE_TEMPL
        fileSize = self.getFileSize()
        output = []
        for repeat in xrange(fileSize // TEMPLATE_SIZE):
            output.append(self.getFilledTemplate(MT_SINGLE_TEMPL, accGen.next(), self.randomStmtNum))
        return EOL.join(output)

    def getOpenningSequence(self, acc, stmNum):
        out = []
        fileSize = self.getFileSize()
        for repeat in xrange(fileSize // TEMPLATE_SIZE):
            if repeat == 0:
                global MT_OPEN_TEMPL
                out.append(self.getFilledTemplate(MT_OPEN_TEMPL, acc, stmNum))
            else:
                global MT_MIDDLE_TEMPL
                out.append(self.getFilledTemplate(MT_MIDDLE_TEMPL, acc, stmNum))
        return EOL.join(out)

    def getMiddleSequence(self, acc, stmNum):
        out = []
        fileSize = self.getFileSize()
        for repeat in xrange(fileSize // TEMPLATE_SIZE):
            global MT_MIDDLE_TEMPL
            out.append(self.getFilledTemplate(MT_MIDDLE_TEMPL, acc, stmNum))
        return EOL.join(out)

    def getFinalSequence(self, acc, stmNum):
        out = []
        fileSize = self.getFileSize()
        for repeat in xrange(fileSize // TEMPLATE_SIZE):
            if repeat == 0:
                global MT_FINAL_TEMPL
                out.append(self.getFilledTemplate(MT_FINAL_TEMPL, acc, stmNum))
            else:
                global MT_MIDDLE_TEMPL
                out.append(self.getFilledTemplate(MT_MIDDLE_TEMPL, acc, stmNum))
        out = reversed(out)
        return EOL.join(out)

    def sequencedMt(self, accGen, sequenceLength):
        assert sequenceLength >= 3
        middleSeqLength = sequenceLength - 2  # openning and closing
        stmNum = self.randomStmtNum
        acc = accGen.next()
        sequence = []
        sequence.append(self.getOpenningSequence(acc, stmNum))
        for i in xrange(middleSeqLength):
            sequence.append(self.getMiddleSequence(acc, stmNum))
        sequence.append(self.getFinalSequence(acc, stmNum))
        return EOL.join(sequence)
