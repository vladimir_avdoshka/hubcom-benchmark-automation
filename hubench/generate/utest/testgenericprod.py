import sys
import os
from unittest import TestCase
from hubench.generate.generic_producer import GenericProducer, ProducerException


class DummyTestProducer(GenericProducer):
    def __call__(self, arg):
        return arg


class DummyTestProducerWithReqCustomAttr(GenericProducer):
    formatSpecificAttrs = ['TEST']

    def __init__(self, formatConfig):
        super(DummyTestProducerWithReqCustomAttr, self).__init__(formatConfig)

    def __call__(self, arg):
        return arg


class TestGenericProd(TestCase):
    def setUp(self):
        class ConfigDict(object):
            pass

        self.config = ConfigDict()
        self.config.NAME = 'MT940'
        self.config.FILES_COUNT = 123
        self.config.MAX_FILES_PER_MINUTE = 123
        self.config.MIN_FILES_PER_MINUTE = 123
        self.config.MAX_FILE_SIZE = 123
        self.config.MIN_FILE_SIZE = 123
        self.config.MIN_SIZE_PERCENT = 123

    def testGenericConfigValidation(self):
        tp = DummyTestProducer(self.config)
        self.assertEqual(tp.config.FILES_COUNT, 123)

    def testGenericConfigiValidationFailed(self):
        class Dummy:
            pass

        self.assertRaises(ProducerException, DummyTestProducer, Dummy)

    def testCustomConfigiValidationFailed(self):
        self.assertRaises(ProducerException, DummyTestProducerWithReqCustomAttr, self.config)

    def testCustomConfigValidation(self):
        self.config.TEST = 'test'
        tp = DummyTestProducerWithReqCustomAttr(self.config)
        self.assertEqual(tp.config.FILES_COUNT, 123)

    def testCallable(self):
        tp = DummyTestProducer(self.config)
        self.assertEqual('assert', tp('assert'))
