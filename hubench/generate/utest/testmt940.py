from unittest import TestCase
from hubench.generate.mt940 import Producer


class TestMt940Generate(TestCase):
    def setUp(self):

        class ConfigDict(object):
            pass

        self.config = ConfigDict()
        self.config.NAME = 'MT940'
        self.config.FILES_COUNT = 0
        self.config.SEQUENCE_DISTRIBUTION = 5
        self.config.SEQUENCE_MAX_SIZE = 3
        self.config.MAX_FILES_PER_MINUTE = 1
        self.config.MIN_FILES_PER_MINUTE = 1
        self.config.MAX_FILE_SIZE = 2000
        self.config.MIN_FILE_SIZE = 500
        self.config.MIN_SIZE_PERCENT = 1

        class AccGen(object):
            def __iter__(self):
                while True:
                    yield 'fakeAcc'

        self.accGen = iter(AccGen())

    def testOneFileGenerate(self):
        self.config.FILES_COUNT = 1
        p = Producer(self.config)
        while p.alive:
            ret = p(self.accGen)
        self.assertTrue(isinstance(ret, basestring))

    def testTwoFilesGenerate(self):
        self.config.FILES_COUNT = 2
        p = Producer(self.config)
        ret = []
        while p.alive:
            ret.append(p(self.accGen))
        self.assertTrue(len(ret) == 2)
        self.assertTrue(isinstance(ret[0], basestring))
        self.assertTrue(isinstance(ret[1], basestring))
