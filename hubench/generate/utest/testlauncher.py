import sys
import os
from unittest import TestCase
from mock import patch
from hubench.generate import launcher
from types import ModuleType


class TestCreateGenerator(TestCase):
    def setUp(self):
        class ConfigDict(object):
            pass

        self.config = ConfigDict()
        self.config.NAME = 'MT940'

    def testGetGenerator(self):
        def formFactory(formName):
            assert formName == 'MT940'
            return lambda x: x

        with patch('hubench.generate.launcher.getProducerModule', formFactory):
            config = launcher.createGenerator(self.config)
            self.assertTrue(config, self.config)


class TestGetProducerModule(TestCase):
    def testGetProdModule(self):
        ret = launcher.getProducerModule('mt940')
        self.assertTrue(type(ret) is ModuleType)
        self.assertEqual(os.path.basename(ret.__file__), 'mt940.py')

    def testGetProducerException(self):
        self.assertFalse('hubench.generate.foo' in sys.modules)
        self.assertRaises(launcher.FileGeneratorException, launcher.getProducerModule, 'hubench.generate.foo')


class TestGetProducer(TestCase):
    def testGetProdModuleWithoutProducerClass(self):
        class Dummy:
            pass

        with patch('hubench.generate.launcher.getProducerModule', lambda x: Dummy):
            self.assertRaises(launcher.FileGeneratorException, launcher.getProducer, 'mt940')

    def testGetProdModuleWithProducerClass(self):
        class Dummy:
            class Producer:
                pass

        with patch('hubench.generate.launcher.getProducerModule', lambda x: Dummy):
            ret = launcher.getProducer('mt940')
            self.assertTrue(ret is Dummy.Producer)
