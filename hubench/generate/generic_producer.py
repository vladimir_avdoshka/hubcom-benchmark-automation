import random


class ProducerException(RuntimeError):
    pass


class GenericProducer(object):
    """
    Represents the interface of producer.
    It is a generator which limits to the maximum of files count specified in format config.
    __call__ method should define files creation logic and return file represented as a text
    """
    formatSpecificAttrs = []  # list here required format specific attributes

    def __init__(self, formatConfig):
        self.__mandatoryAttrs = ['NAME', 'FILES_COUNT', 'MAX_FILES_PER_MINUTE', 'MIN_FILES_PER_MINUTE',
                                 'MAX_FILE_SIZE', 'MIN_FILE_SIZE', 'MIN_SIZE_PERCENT']
        self.config = self.configValidation(formatConfig)
        self.counter = 0  # mandatory atrr, should be handeled in __call__ method

    def configValidation(self, config):
        missing = []
        for attr in self.__mandatoryAttrs + self.formatSpecificAttrs:
            if not hasattr(config, attr):
                missing.append(attr)
        if missing:
            msg = 'Following format param(s) missing: `{}`.'.format(', '.join(missing))
            if hasattr(config, 'NAME'):
                msg += 'Format name: {}.'.format(config.NAME)
            else:
                msg += 'Format undefined.'
            raise ProducerException(msg)
        return config

    def __call__(self, accountGenerator):
        """
        define here a logic of file creation. Also take care of self.counter,
        it indicates when the generator is exhausted
        :param accountGenerator: infinite generator of accounts
        :return: text as a String representing the file content
        """
        raise NotImplementedError

    def getFileSize(self):
        return random.randint(self.config.MIN_FILE_SIZE, self.config.MAX_FILE_SIZE)

    @property
    def alive(self):
        return True if self.counter < self.config.FILES_COUNT else False
