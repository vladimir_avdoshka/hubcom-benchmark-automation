from .flow_controls import (dataRoot, patchConfig,
                            pyroServer, enabledScheduledTask,
                            waitTillProcessingComplete)


def bsiTest(appConfig, huConfig, bsiPusher):
    with dataRoot(huConfig.DATA_ROOT, appConfig.DATA_ROOT_SAMPLE, appConfig.DATA_ROOT_BKP) as dataRootPath:
        with patchConfig(huConfig.CONFIG_PATH, huConfig.toDict()):
            with pyroServer(huConfig.PYRO_START_CMD, huConfig.HU_SCRIPTS):
                fileDispatchingProcesses = bsiPusher()  # processes to push files started
                with enabledScheduledTask(appConfig.SCHEDULED_STATEMENT_TASK, appConfig.ASSETS_FOLDER):
                    waitTillProcessingComplete(dataRootPath, huConfig.PROTOCOL_TO_RELATIVE_INBOX,
                                               fileDispatchingProcesses)
