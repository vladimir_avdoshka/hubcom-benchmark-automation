from zipfile import ZipFile, ZIP_DEFLATED
import os
import errno
import time


def ensureDir(*dirs):
    for dir in dirs:
        if not os.path.exists(dir):
            try:
                os.makedirs(dir)
            except OSError as ex:
                if ex.errno == errno.EEXIST or os.path.isdir(dir):
                    pass
                else:
                    raise


def zipFromTo(from_, to):
    with ZipFile(to, 'w', compression=ZIP_DEFLATED) as bkpZip:
        for root, _, files in os.walk(from_):
            for fileName in files:
                filePath = os.path.join(root, fileName)
                arcname = filePath.replace(from_, '')
                bkpZip.write(filePath, arcname)


def backupFolder(from_, to):
    zipPath = os.path.join(to, 'data_%s.zip' % time.strftime('%Y%m%d%H%M%S'))
    zipFromTo(from_, zipPath)


def waitForLock(lockPath):
    # logging.info("waiting for %s lock...", lockPath)
    treshold = 5
    lockPath = os.path.join(lockPath)
    while treshold:
        if not os.path.exists(lockPath):
            treshold -= 1
        time.sleep(0.5)
        # logging.info("%s lock released", lockPath)
