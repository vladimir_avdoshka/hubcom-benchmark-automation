import os
import time
import shutil
import subprocess
from contextlib import contextmanager
from zipfile import ZipFile
from .tools import ensureDir, backupFolder, waitForLock


class HubError(RuntimeError):
    pass


@contextmanager
def dataRoot(workRoot, etalon, bkpFolder):
    # to clean work dirs?
    ensureDir(workRoot)
    ensureDir(bkpFolder)
    if os.path.isdir(workRoot):
        backupFolder(workRoot, bkpFolder)
        shutil.rmtree(workRoot)
    with ZipFile(etalon, 'r') as dataRootZip:
        dataRootZip.extractall(os.path.dirname(os.path.normpath(workRoot))) # data root sample archive contains `data` as root element
    try:
        yield workRoot
    finally:
        backupFolder(workRoot, bkpFolder)
        try:
            shutil.rmtree(workRoot)
        except OSError:
            time.sleep(3)
            shutil.rmtree(workRoot)


@contextmanager
def patchConfig(configPath, paramsToPatch):
    """
    :param configPath: path to hubcom config
    :param paramsToPatch: Huplatform config object
    :return:
    """
    # logging.info("patching config at '%s' with following params %s", configPath, str(paramsToPatch))
    configBkpPath = configPath + '.bkp'

    # logging.info("config patched")

    shutil.copy(configPath, configBkpPath)

    with open(configPath) as conf:
        configContent = conf.read()

    with open(configPath, 'w') as conf:
        conf.write(configContent)
        for name, value in paramsToPatch.iteritems():
            conf.write("\n%s = %s" % (name, repr(value)))

    # logging.info("config patched")

    try:
        yield
    finally:
        with open(configPath, 'w') as conf:
            conf.write(configContent)
        # logging.info("config restored to previous state")
        os.unlink(configBkpPath)


@contextmanager
def pyroServer(startCmd, huScriptsHome):
    # logging.info("starting Pyro...")
    p = subprocess.Popen(startCmd, cwd=huScriptsHome)

    start = time.time()
    started = False
    # waiting to pyro start
    # server started when processor module can be imported, i.e. selfcheck processor available
    while time.time() - start < 60:
        if subprocess.call('python -c "import processor"') == 0:
            started = True
            break
        time.sleep(0.5)

    if not started:
        raise HubError("Can't start Pyro server")

    try:
        yield
    finally:
        p.kill()
        # logging.info("Pyro stopped")


def getXmlPathForTaskName(assetsFolder, taskName):
    taskXmlName = os.path.join(assetsFolder, 'tasks', "%s.xml" % taskName)
    if not os.path.exists(taskXmlName):
        raise HubError("Can't find task xml at %s" % taskXmlName)
    return taskXmlName


def createScheduledTask(taskName, assetsFolder):
    os.system("schtasks /Create /TN %s /XML %s" % (taskName, getXmlPathForTaskName(assetsFolder, taskName)))


def deleteScheduledTask(taskName):
    os.system("schtasks /Delete /TN %s /F" % taskName)


@contextmanager
def enabledScheduledTask(taskName, assetsFolder):
    createScheduledTask(taskName, assetsFolder)
    os.system('schtasks /Change /ENABLE /TN %s' % taskName)
    try:
        yield
    finally:
        os.system('schtasks /Change /DISABLE /TN %s' % taskName)
        deleteScheduledTask(taskName)


@contextmanager
def launchedProcesses(cmds, huScriptsHome):
    if cmds:
        # logging.info("starting following processes:\n%s", '\n'.join(cmds))
        ps = [subprocess.Popen(cmd, cwd=huScriptsHome) for cmd in cmds]
        try:
            yield
        finally:
            [p.kill() for p in ps]
            # logging.info("following processes were finished:\n%s", '\n'.join(cmds))
    else:
        yield


def waitTillProcessingComplete(targetDataRootPath, protoToRelevativeInbox, fileDispatchingProcesses):
    # logging.info("waiting for files being processed...")
    [p.join() for p in fileDispatchingProcesses]  # wait till all files will be pushed
    inboxFolders = [os.path.join(targetDataRootPath, inbox) for inbox in protoToRelevativeInbox.values()]
    haveFiles = lambda folder: any(files for _, _, files in os.walk(folder))
    isFilesProcessed = lambda: not any(haveFiles(inbox) for inbox in inboxFolders)
    while not isFilesProcessed():
        time.sleep(5)
    # logging.info("all files have been processed")
    waitForLock(os.path.join(targetDataRootPath, 'lock', 'Statement.lck'))
    waitForLock(os.path.join(targetDataRootPath, 'lock', 'BSICollectorLauncher.lck'))
    # logging.info("processing complete")
